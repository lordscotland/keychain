ARCHS = arm64

include theos/makefiles/common.mk

TOOL_NAME = keychain
keychain_FILES = main.c
keychain_FRAMEWORKS = CoreFoundation Security
keychain_CODESIGN_FLAGS = -Skeychain.xml

include $(THEOS_MAKE_PATH)/tool.mk
