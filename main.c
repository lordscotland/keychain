#import <CoreFoundation/CoreFoundation.h>
#import <Security/Security.h>

#define OUTPUT_FS '\t'
#define OUTPUT_RS '\n'

static void fputCFType(CFTypeRef object,FILE* fh) {
  if(!object){
    fputs("NULL",fh);
    return;
  }
  const CFTypeID typeID=CFGetTypeID(object);
  if(typeID==CFNumberGetTypeID()){
    if(CFNumberIsFloatType(object)){
      double value;
      Boolean ok=CFNumberGetValue(object,kCFNumberDoubleType,&value);
      fprintf(fh,"%.6g",value);
      if(!ok){fputc('?',fh);}
    }
    else {
      long long value;
      Boolean ok=CFNumberGetValue(object,kCFNumberLongLongType,&value);
      fprintf(fh,"%lld",value);
      if(!ok){fputc('?',fh);}
    }
  }
  else if(typeID==CFDataGetTypeID()){
    const char* const vchars="ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
    const UInt8* bytes=CFDataGetBytePtr(object);
    CFIndex nbytes=CFDataGetLength(object);
    for (CFIndex i=0;i<nbytes;){
      int i0=bytes[i]>>2,i1=(bytes[i++]&0x3)<<4,i2,i3;
      if(i<nbytes){
        i1|=bytes[i]>>4;
        i2=(bytes[i++]&0xF)<<2;
        if(i<nbytes){
          i2|=bytes[i]>>6;
          i3=bytes[i++]&0x3F;
        }
        else {i3=64;}
      }
      else {i2=i3=64;}
      char buf[]={vchars[i0],vchars[i1],vchars[i2],vchars[i3]};
      fwrite(buf,1,sizeof(buf),fh);
    }
  }
  else {
    CFStringRef description=(typeID==CFStringGetTypeID())?
     NULL:(object=CFCopyDescription(object));
    CFIndex pos=0,remain=CFStringGetLength(object);
    while(remain>0){
      UInt8 bytes[4096];
      CFIndex nbytes,nconv=CFStringGetBytes(object,CFRangeMake(pos,remain),
       kCFStringEncodingUTF8,'?',false,bytes,sizeof(bytes),&nbytes);
      fwrite(bytes,1,nbytes,fh);
      pos+=nconv;
      remain-=nconv;
    }
    if(description){CFRelease(description);}
  }
}
static CFDataRef copyPersistentRef(FILE* fh) {
  CFMutableDataRef ref=NULL;
  while(1){
    UInt32 byte;
    UInt8 buf[32];
    CFIndex len=0;
    while(len<sizeof(buf) && fscanf(fh,"%02X",&byte)>0){buf[len++]=byte;}
    if(!len){break;}
    if(!ref){ref=CFDataCreateMutable(NULL,0);}
    CFDataAppendBytes(ref,buf,len);
    if(len<sizeof(buf)){break;}
  }
  fscanf(fh,"%*[^\n]");
  fgetc(fh);
  return ref;
}
static void fputPersistentRef(CFDataRef ref,FILE* fh) {
  const UInt8* bytes=CFDataGetBytePtr(ref);
  for (CFIndex i=0,nbytes=CFDataGetLength(ref);i<nbytes;i++){
    fprintf(fh,"%02X",bytes[i]);
  }
}
static void printKeyValue(const void* key,const void* value,void* context) {
  fputc('-',context);
  fputc(OUTPUT_FS,context);
  fputCFType(key,context);
  fputc(OUTPUT_FS,context);
  fputCFType(value,context);
  fputc(OUTPUT_RS,context);
}
static void dumpItems(CFStringRef itemClass,CFStringRef* keys,CFIndex nkeys) {
  CFDictionaryRef query=CFDictionaryCreate(NULL,
   (CFTypeRef[]){kSecClass,kSecAttrSynchronizable,kSecMatchLimit,
   kSecReturnAttributes,kSecReturnPersistentRef},
   (CFTypeRef[]){itemClass,kSecAttrSynchronizableAny,kSecMatchLimitAll,
   kCFBooleanTrue,kCFBooleanTrue},5,NULL,&kCFTypeDictionaryValueCallBacks);
  CFArrayRef items;
  OSStatus status=SecItemCopyMatching(query,(CFTypeRef*)&items);
  CFRelease(query);
  if(status==errSecSuccess){
    const CFIndex count=CFArrayGetCount(items);
    for (CFIndex i=0;i<count;i++){
      CFDictionaryRef item=CFArrayGetValueAtIndex(items,i);
      CFDataRef ref=CFDictionaryGetValue(item,kSecValuePersistentRef);
      fputPersistentRef(ref,stdout);
      fputc(OUTPUT_FS,stdout);
      CFBooleanRef sync=CFDictionaryGetValue(item,kSecAttrSynchronizable);
      if(sync && CFBooleanGetValue(sync)){fputc('*',stdout);}
      fputCFType(itemClass,stdout);
      fputc(OUTPUT_FS,stdout);
      fputCFType(CFDictionaryGetValue(item,kSecAttrAccessGroup),stdout);
      for (CFIndex j=0;j<nkeys;j++){
        fputc(OUTPUT_FS,stdout);
        fputCFType(CFDictionaryGetValue(item,keys[j]),stdout);
      }
      fputc(OUTPUT_RS,stdout);
    }
    CFRelease(items);
  }
  else {
    fputs("SecItemCopyMatching(",stderr);
    fputCFType(itemClass,stderr);
    fprintf(stderr,"): %d%c",(int)status,OUTPUT_RS);
  }
}

int main(int argc,char** argv) {
  if(argc<2){
    CFStringRef certkeys[]={kSecAttrCertificateType,kSecAttrIssuer,kSecAttrSerialNumber};
    dumpItems(kSecClassGenericPassword,(CFStringRef[]){
     kSecAttrAccount,kSecAttrService},2);
    dumpItems(kSecClassInternetPassword,(CFStringRef[]){
     kSecAttrAccount,kSecAttrSecurityDomain,kSecAttrServer,kSecAttrProtocol,
     kSecAttrAuthenticationType,kSecAttrPort,kSecAttrPath},7);
    dumpItems(kSecClassCertificate,certkeys,3);
    dumpItems(kSecClassKey,(CFStringRef[]){
     kSecAttrKeyClass,kSecAttrKeyType,kSecAttrApplicationLabel,kSecAttrApplicationTag,
     kSecAttrKeySizeInBits,kSecAttrEffectiveKeySize},6);
    dumpItems(kSecClassIdentity,certkeys,3);
  }
  else if(strcmp(argv[1],"-p")==0){
    CFMutableDictionaryRef query=CFDictionaryCreateMutable(NULL,3,
     NULL,&kCFTypeDictionaryValueCallBacks);
    CFDictionarySetValue(query,kSecReturnAttributes,kCFBooleanTrue);
    CFDictionarySetValue(query,kSecReturnData,kCFBooleanTrue);
    while(!feof(stdin)){
      CFDataRef ref=copyPersistentRef(stdin);
      if(!ref){continue;}
      CFDictionarySetValue(query,kSecValuePersistentRef,ref);
      CFDictionaryRef item;
      OSStatus status=SecItemCopyMatching(query,(CFTypeRef*)&item);
      if(status==errSecSuccess){
        fputPersistentRef(ref,stdout);
        fputc(OUTPUT_RS,stdout);
        CFDictionaryApplyFunction(item,printKeyValue,stdout);
        CFRelease(item);
      }
      else {
        fputPersistentRef(ref,stderr);
        fprintf(stderr,"%c%d%c",OUTPUT_FS,(int)status,OUTPUT_RS);
      }
      CFRelease(ref);
    }
    CFRelease(query);
  }
  else if(strcmp(argv[1],"-d")==0){
    CFStringRef classes[5]={kSecClassGenericPassword,kSecClassInternetPassword,
     kSecClassCertificate,kSecClassKey,kSecClassIdentity};
    CFMutableDictionaryRef query=CFDictionaryCreateMutable(NULL,2,
     NULL,&kCFTypeDictionaryValueCallBacks);
    if(2<argc){
      for (int i=2;i<argc;i++){
        CFStringRef group=CFStringCreateWithCString(NULL,argv[i],kCFStringEncodingUTF8);
        CFDictionarySetValue(query,kSecAttrAccessGroup,group);
        CFRelease(group);
        fputs(argv[i],stdout);
        for (int j=0;j<5;j++){
          CFDictionarySetValue(query,kSecClass,classes[j]);
          printf("%c%d",OUTPUT_FS,(int)SecItemDelete(query));
        }
        fputc(OUTPUT_RS,stdout);
      }
    }
    else {
      while(!feof(stdin)){
        CFDataRef ref=copyPersistentRef(stdin);
        if(!ref){continue;}
        CFDictionarySetValue(query,kSecValuePersistentRef,ref);
        fputPersistentRef(ref,stdout);
        CFRelease(ref);
        printf("%c%d%c",OUTPUT_FS,(int)SecItemDelete(query),OUTPUT_RS);
      }
    }
    CFRelease(query);
  }
  else {
    fprintf(stderr,"Usage: %s [-p|-d [<groupIDs>]]\n",argv[0]);
    return -1;
  }
}
